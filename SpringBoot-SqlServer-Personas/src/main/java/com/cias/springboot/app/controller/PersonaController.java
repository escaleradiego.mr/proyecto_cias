package com.cias.springboot.app.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cias.springboot.app.model.dao.InterfacePersonaDao;

@Controller
public class PersonaController {
	
	@Autowired
	private InterfacePersonaDao personaDao;
	
	@RequestMapping({"/","index"})
	public String desplegarPersonas(Model modelo) {
		modelo.addAttribute("personas",personaDao.listaPersonas());
		return "index";
	}
	
	@ModelAttribute("estatusMap")
	public Map<String,String> estatusMap(){
		Map<String,String> estatus = new HashMap<String,String>();
		estatus.put("A", "Activo");
		estatus.put("I", "Inactivo");		
		return estatus;
	}
}
