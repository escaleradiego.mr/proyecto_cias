package com.cias.springboot.app.model.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cias.springboot.app.model.Persona;

@Repository
public interface InterfacePersonaRestDao extends JpaRepository<Persona, Integer>{

}
