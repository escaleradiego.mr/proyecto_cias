package com.cias.springboot.app.model.dao;

import java.util.List;

import com.cias.springboot.app.model.Persona;


public interface InterfacePersonaDao {

	public List<Persona> listaPersonas();
	
	public void guardarPersona(Persona persona);
	
	public Persona consultarPersona(Integer id);
	
	public void eliminarPersona(Integer id);
}
