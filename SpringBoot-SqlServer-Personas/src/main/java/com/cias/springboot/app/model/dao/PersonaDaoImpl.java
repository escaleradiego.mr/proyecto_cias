package com.cias.springboot.app.model.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.cias.springboot.app.model.Persona;

@Repository("personaDaoJPA")
public class PersonaDaoImpl implements InterfacePersonaDao{
	
	@Value("${constante.clientedaoimpl.filtroestatus}")
	private String filtroEstatus;
	
	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	@Override
	public List<Persona> listaPersonas() {
		return em.createQuery(" from Persona where Estatus = '" + filtroEstatus + "'").getResultList();
	}

	@Override
	@Transactional
	public void guardarPersona(Persona persona) {
		if(persona.getId() != null )	{
			em.merge(persona);
		}else {
			em.persist(persona);
		}
	}

	@Override
	@Transactional(readOnly = true)
	public Persona consultarPersona(Integer id) {
		return em.find(Persona.class, id);
	}

	@Override
	@Transactional
	public void eliminarPersona(Integer id) {
		em.remove(consultarPersona(id));		
	}

}
